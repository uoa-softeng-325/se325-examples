package se325.lecture02.basicgreetings.client;

import se325.lecture02.Keyboard;
import se325.lecture02.basicgreetings.shared.GreetingService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HelloWorldClient {

    public static void main(String[] args) {

        try {
            Registry lookupService = LocateRegistry.getRegistry("localhost", 8080);
            GreetingService service = (GreetingService) lookupService.lookup("greetingService");

            String name = Keyboard.prompt("What is your name?");

            System.out.println(service.getGreeting(name));
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

}
