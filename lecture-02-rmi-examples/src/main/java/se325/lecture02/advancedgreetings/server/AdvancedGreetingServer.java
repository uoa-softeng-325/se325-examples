package se325.lecture02.advancedgreetings.server;

import se325.lecture02.advancedgreetings.shared.GreetingServiceFactory;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class AdvancedGreetingServer {

    public static void main(String[] args) throws RemoteException {

        Registry lookupService = LocateRegistry.createRegistry(8080);

        GreetingServiceFactory greetingFactory = new GreetingServiceFactoryServant();

        lookupService.rebind("greetingFactory", greetingFactory);

        System.out.println("Server running on port 8080!");

    }
}
