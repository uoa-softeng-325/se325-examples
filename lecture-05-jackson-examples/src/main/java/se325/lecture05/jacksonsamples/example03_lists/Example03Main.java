package se325.lecture05.jacksonsamples.example03_lists;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class Example03Main {

    public static void main(String[] args) throws IOException {

        Type fireType = new Type("Fire");
        Type flyingType = new Type("Flying");

        Pokemon charizard = new Pokemon("Charizard", fireType, flyingType);

        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(charizard);
        System.out.println("Charizard json: " + json);

        Pokemon deserialized = mapper.readValue(json, Pokemon.class);

    }

}